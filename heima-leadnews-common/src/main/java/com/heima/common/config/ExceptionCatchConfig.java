package com.heima.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */

@Configuration
@ComponentScan("com.heima.common.exception")
public class ExceptionCatchConfig {
}
