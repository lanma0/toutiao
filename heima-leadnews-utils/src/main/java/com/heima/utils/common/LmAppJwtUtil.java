package com.heima.utils.common;

import io.jsonwebtoken.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */
public class LmAppJwtUtil  {

    // TOKEN的有效期一天（S）
    private static final int TOKEN_TIME_OUT = 3600;
    // 加密KEY
//    private static final String TOKEN_ENCRY_KEY = MD5Utils.encode("Iam1anma0");
    private static final String TOKEN_ENCRY_KEY = "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY";
    // 最小刷新间隔(S)
    private static final int REFRESH_TIME = 300;

    public static String getToken(Long id){
        //1.先进行header的封装
        Map<String,Object> mapHeader = new HashMap<>();
        mapHeader.put("alg","HS512");
        mapHeader.put("type","JWT");

        //1.先进行calim的封装
        Map<String,Object> cliaimMaps = new HashMap<>();
        cliaimMaps.put("id",id);
        String token = Jwts.builder().setClaims(cliaimMaps)
                .setHeader(mapHeader)
                .setId(UUID.randomUUID().toString())
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_TIME_OUT * 1000)) //1.1000小时过期
                .signWith(SignatureAlgorithm.HS512, generalKey())
                .setIssuedAt(new Date()) //签发时间
                .setSubject("system")  //说明
                .setIssuer("heima") //签发者信息
                .setAudience("app")  //接收用户
                .compressWith(CompressionCodecs.GZIP)//数据压缩方式
                .compact();

        return token;

    }


    //还在探究这是个什么玩意
    public static SecretKey generalKey() {
        byte[] encodedKey = Base64.getEncoder().encode(TOKEN_ENCRY_KEY.getBytes());
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

    /**
     * 获取token中的claims信息
     *
     * @param token
     * @return
     */
    private  static Jws<Claims> getJws(String token){
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(generalKey())
                .parseClaimsJws(token);
        return claimsJws;
    }

    /**
     * 获取payload body信息
     *
     * @param token
     * @return
     */
    public static Claims getClaimsBody(String token) {
        try {
            return getJws(token).getBody();
        }catch (ExpiredJwtException e){
            return null;
        }
    }
    /**
     * 获取hearder body信息
     *
     * @param token
     * @return
     */
    public static JwsHeader getHeaderBody(String token) {
        return getJws(token).getHeader();
    }


    //校验token,知识判断是否有效  -1：有效，0：有效，1：错误，2：过期
    public static  int verifyToken(Claims claims){
        if(claims==null){
            return 1;
        }
        try {
            boolean before = claims.getExpiration()//获取过期时间
                    .before(new Date());
            System.out.println("claims.getExpiration()::"+claims.getExpiration());
            if((claims.getExpiration().getTime()-System.currentTimeMillis())>REFRESH_TIME*1000){
                return -1;
            }else {
                return 0;
            }
        }catch (ExpiredJwtException ex) {
            return 1;
        }catch (Exception e){
            return 2;
        }
    }
    public static void main(String[] args) {
       /* Map map = new HashMap();
        map.put("id","11");*/
//        System.out.println(LmAppJwtUtil.getToken(1102L));
//        Jws<Claims> jws = LmAppJwtUtil.getJws(
//                "eyJ6aXAiOiJHWklQIiwidHlwZSI6IkpXVCIsImFsZyI6IkhTNTEyIn0.H4sIAAAAAAAAAC2LSwrDMAwF76J1DJYqR05v41-pCwGDHGgpuXsV6G7mDe8LemS4g350th0WSEc1TWMYd1XjZ-t7uswCoqcF2nsYrhS9-EjBUpr_YRW6htfs9gyCJLf6cFy4Os5YXGYhV7YYuOImITU4f4PG2ZWDAAAA.4AY-BzHf7F24XpEyfaRa_WiELVo1JJeXoQ5q6DcqbbqYt39S0gW7P02vxiMoymOrEeWN2J2-idxmn_A7c0JCJw"
//        );
//        Claims claims = jws.getBody();
//        System.out.println(claims);
//        System.out.println(claims.get("id"));
        String s = new String(LmAppJwtUtil.generalKey().getEncoded());
        System.out.println(s);

    }

}


