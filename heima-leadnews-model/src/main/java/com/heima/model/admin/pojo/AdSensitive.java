package com.heima.model.admin.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */

@Data
@TableName("ad_sensitive")
public class AdSensitive implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;



        //敏感词
    @TableField("sensitives")
    private String sensitives;


    //创建时间和更新时间，可以用mybatis的配置，自动配置
    @TableField("created_time")
    private Date createdTime;
}
