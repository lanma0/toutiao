package com.heima.admin.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

    @Bean
    public Docket buildDocket(){

        return   new Docket(DocumentationType.SWAGGER_2).apiInfo(buildApiInfo())
                // 要扫描的API(Controller)基础包
                .select().apis(RequestHandlerSelectors.basePackage("com.heima"))
                .paths(PathSelectors.any())
                .build().groupName("heima-c10wn");
    }

    private ApiInfo buildApiInfo() {
        Contact contact = new Contact("c10wn", "https://www.baidu.com", "878403789@qq.com");//作者信息

//        return new ApiInfoBuilder()
//                .title("黑马头条-平台管理API文档")
//                .description("平台管理服务api")
//                .contact(contact)
//               .version("1.0.0").build();
//    }

        return new ApiInfo("黑马头条-平台管理API文档"
                , "平台管理服务api"
                , "1.0",
                "https://www.bilibili.com/video/BV1PE411i7CV?p=48"
                , contact, "Apache 2.0"
                , "http://www.apache.org/licenses/LICENSE-2.0"
                , new ArrayList());
    }
}
