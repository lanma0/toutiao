package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */


public interface AdSensitiveService extends IService<AdSensitive> {

    //自定义分页查询接口
    public ResponseResult  list(SensitiveDto dto);

    /**
     * 新增
     * @param adSensitive
     * @return
     */
    public ResponseResult insert(AdSensitive adSensitive);

    public ResponseResult update(AdSensitive adSensitive);

    public ResponseResult deleteById(Integer id);
}
