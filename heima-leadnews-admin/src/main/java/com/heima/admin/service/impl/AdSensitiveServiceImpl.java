package com.heima.admin.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdSensitiveMapper;
import com.heima.admin.service.AdSensitiveService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.mchange.lang.IntegerUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */

@Service
public class AdSensitiveServiceImpl extends ServiceImpl<AdSensitiveMapper, AdSensitive> implements AdSensitiveService {
    @Override
    public ResponseResult list(SensitiveDto dto) {

        //1.校验参数
        if (ObjectUtil.isEmpty(dto)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();

        //2.模糊查询
        Page page = new Page<>(dto.getPage(),dto.getSize());
        LambdaQueryWrapper<AdSensitive> wrapper =new LambdaQueryWrapper<>();

        //2.1判断是否存在参数name
        if (StringUtils.isNotBlank(dto.getName())) {
            //这里的参数是：第一组代表那一列，第二组代表需要模糊插叙你的参数
            wrapper.like(AdSensitive::getSensitives,dto.getName());
        }
        //这个page 是继承了serviceimpl自带的方法；
        IPage result = page(page, wrapper);


        //多态方法返回ResponseResult
        ResponseResult responseResult = new PageResponseResult(dto.getPage(), dto.getSize(), Convert.toInt(result.getTotal()));
        responseResult.setData(result);

        return responseResult;
    }

    @Override
    public ResponseResult insert(AdSensitive adSensitive) {
        //1.校验参数
        if (ObjectUtil.isEmpty(adSensitive)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.判断里面的sensitives 是否为空
        if (!StringUtils.isNotEmpty(adSensitive.getSensitives())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.保存
        adSensitive.setCreatedTime(new Date(System.currentTimeMillis()));
        save(adSensitive);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult update(AdSensitive adSensitive) {
        //1.判断参数 全不为空
        if (!ObjectUtil.isAllNotEmpty(adSensitive.getId(),adSensitive.getSensitives())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //更新
        update(adSensitive);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }

    @Override
    public ResponseResult deleteById(Integer id) {

        if (ObjectUtil.isNull(id)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        }
        //1.判断删除的Id是否可以不存在
        AdSensitive adSensitive = getById(id);
        if (ObjectUtil.isEmpty(adSensitive)){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        removeById(id);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}

