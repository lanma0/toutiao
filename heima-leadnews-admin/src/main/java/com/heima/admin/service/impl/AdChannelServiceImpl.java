package com.heima.admin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.service.AdChannelService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */

//这里要先继承myplus基本实现累 ServiceImpl 再实现我们自定义的类。
@Service
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel> implements AdChannelService {


    @Autowired
    private AdChannelMapper adChannelMapper;

    @Override
    public ResponseResult findByNameAndPage(ChannelDto dto) {

        //1.参数检测
        if (ObjectUtil.isEmpty(dto)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }


        //设置分页参数
        dto.checkParam();//不知道这么做的意义。

        //2.安装名称模糊分页查询

        Page <AdChannel> adChannelIpage = new Page(dto.getPage(),dto.getSize());

        LambdaQueryWrapper<AdChannel> lqw=new LambdaQueryWrapper();
        if (StringUtils.isNotBlank(dto.getName())) {
            lqw.like(AdChannel::getName, dto.getName());
        }
//使用Page进行查询。之前没了解过。
        IPage result = page(adChannelIpage, lqw);
        //设置命中的总条数；
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getSize(), (int) result.getTotal());
        //讲结果信息封装到data中
        pageResponseResult.setData(result.getRecords());

        //3.结果封装
        return pageResponseResult;
    }

    @Override
    public ResponseResult insert(AdChannel channel) {
        //校验参数
        if(null == channel){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        channel.setCreatedTime(new Date(System.currentTimeMillis()));

        //调用ServiceImpl里面封装的方法。
        this.save(channel);

        //返回结果参数
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult update(AdChannel channel) {
        //校验参数
        if(null == channel || channel.getId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //调用ServiceImpl里面封装的方法。
        this.updateById(channel);
        //返回结果参数
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult deleteById(Integer id) {
        //1.检查参数是否合格
        if (null==id) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //通过数据库查找数据；
        AdChannel adChannel = getById(id);
        //判断频道的状态是否有效，有效则不能删除，返回参数

        if (adChannel.getStatus()){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"频道状态为有效，不能删除");
        }

        removeById(id);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
