package com.heima.admin.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdUserMapper;
import com.heima.admin.service.UserLoginService;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.utils.common.LmAppJwtUtil;
import com.heima.utils.common.MD5Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */
@Service
//事务管理，如果错误进行回滚
@Transactional
public class UserLoginServiceImpl extends ServiceImpl<AdUserMapper, AdUser> implements UserLoginService {
    @Override
    public ResponseResult login(String username, String password) {
        //1.参数检查
        if (!StrUtil.isAllNotBlank(username,password)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "用户名或密码不能为空");
        }
        //2.数据库查询
        QueryWrapper wrapper = new QueryWrapper<AdUser>();
        wrapper.eq("name",username);
        List<AdUser> list = list(wrapper);
        System.out.println(list);
        System.out.println(list.size());
        //如果这个

        if (null==list &&list.size() == 1) {
            //拿到该对象
            AdUser adUser = list.get(0);
            String encodePwd = MD5Utils.encodeWithSalt(password, adUser.getSalt());
            if (StrUtil.equals(encodePwd,adUser.getPassword())) {
                //如果登录成功则讲 adUser和token对象返回给到前端
                adUser.setPassword("");
                adUser.setSalt("");
                Map<String,Object> map =new HashMap<>();
                map.put("user",adUser);
                map.put("token", LmAppJwtUtil.getToken(adUser.getId().longValue()));
                return ResponseResult.okResult(map);
            }else {
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
        }else {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "用户不存在");
        }
    }
}
