package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.common.dtos.ResponseResult;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */
public interface UserLoginService extends IService<AdUser> {

    ResponseResult login(String username,String password);
}
