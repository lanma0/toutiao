package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.stereotype.Service;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */


public interface  AdChannelService extends IService<AdChannel> {

    //自定义分页查询接口
    public ResponseResult findByNameAndPage(ChannelDto dto);

    /**
     * 新增
     * @param channel
     * @return
     */
    public ResponseResult insert(AdChannel channel);

    public ResponseResult update(AdChannel channel);

    public ResponseResult deleteById(Integer id);
}
