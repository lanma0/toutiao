package com.heima.admin.controller.v1;

import com.heima.admin.service.UserLoginService;
import com.heima.api.admin.LoginControllerApi;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */

@RestController
@RequestMapping("/login")
public class LoginController implements LoginControllerApi {
    @Autowired
    private UserLoginService userLoginService ;

    @Override
    @PostMapping("/in")
    public ResponseResult login(@RequestBody Map<String, String> parameter) {
        String name = parameter.get("name");
        String password = parameter.get("password");
        ResponseResult login = userLoginService.login(name, password);
        return login;
    }
}
