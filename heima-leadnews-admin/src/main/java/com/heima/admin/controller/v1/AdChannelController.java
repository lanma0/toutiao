package com.heima.admin.controller.v1;

import com.heima.admin.service.AdChannelService;
import com.heima.api.admin.AdChannelControllerApi;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */

@RestController
@RequestMapping("/api/v1/channel")
public class AdChannelController implements AdChannelControllerApi {

    @Autowired
    private AdChannelService channelService;
    @Override
    @PostMapping("/list")
    public ResponseResult findByNameAndPage(@RequestBody ChannelDto dto) {
        ResponseResult responseResult = channelService.findByNameAndPage(dto);
        return responseResult;
    }

    @Override
    @PostMapping("/save")
    public ResponseResult save(@RequestBody  AdChannel channel) {
        return channelService.insert(channel);
    }

    @Override
    @PostMapping("/update")
    public ResponseResult update(@RequestBody  AdChannel channel) {
        return channelService.update(channel);
    }

    @Override
    @GetMapping("/del/{id}")
    public ResponseResult deleteById(@PathVariable Integer id) {
        return channelService.deleteById(id);
    }

}
