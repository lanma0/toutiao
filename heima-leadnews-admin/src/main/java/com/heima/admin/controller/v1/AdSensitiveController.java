package com.heima.admin.controller.v1;

import com.heima.admin.service.AdChannelService;
import com.heima.admin.service.AdSensitiveService;
import com.heima.api.admin.AdChannelControllerApi;
import com.heima.api.admin.AdSensitiveControllerApi;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.admin.pojo.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */

@RestController
@RequestMapping("/api/v1/sensitive")
public class AdSensitiveController implements AdSensitiveControllerApi {

    @Autowired
    private AdSensitiveService sensitiveService;
    @PostMapping("/list")
    @Override
    public ResponseResult list(SensitiveDto dto) {
        ResponseResult responseResult = sensitiveService.list(dto);
        return responseResult;
    }

    @Override
    @PostMapping("/save")
    public ResponseResult save(@RequestBody AdSensitive adSensitive) {
        return sensitiveService.insert(adSensitive);
    }

    @Override
    @PostMapping("/update")
    public ResponseResult update(@RequestBody  AdSensitive adSensitive) {
        return sensitiveService.update(adSensitive);
    }

    @Override
    @GetMapping("/del/{id}")
    public ResponseResult deleteById(@PathVariable Integer id) {
        return sensitiveService.deleteById(id);
    }

}
