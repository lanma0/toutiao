package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojo.AdSensitive;
import org.apache.ibatis.annotations.Mapper;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */
@Mapper
public interface AdSensitiveMapper extends BaseMapper<AdSensitive> {
}
