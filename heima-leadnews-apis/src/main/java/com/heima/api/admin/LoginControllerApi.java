package com.heima.api.admin;

import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/4
 * @描述
 */
public interface LoginControllerApi {

    ResponseResult login(Map<String,String> parameter);

}
