package com.heima.api.admin;

import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojo.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @创建人 1anma0
 * @创建时间 2021/8/3
 * @描述
 */

@Api(value = "频道管理",tags = "channel",description = "频道管理API")
public interface AdChannelControllerApi {
    @ApiOperation(value = "频道分页列表查询")
    public ResponseResult findByNameAndPage(ChannelDto dto);

    /**
     * 新增
     * @param channel
     * @return
     */
    @ApiOperation(value = "新增频道")
    public ResponseResult save(AdChannel channel);

    @ApiOperation(value = "修改频道，修改状态")
    public ResponseResult update(AdChannel channel);
    @ApiOperation(value = "删除频道")
    public ResponseResult deleteById(Integer id);

}
